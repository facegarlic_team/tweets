<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <form action="/register" method="POST">
        {{ csrf_field() }}
        <ul>
            <li>
                <label for="">Email</label>
                <input type="text" name="email">
            </li>
            <li>
                <label for="">Password:</label>
                <input type="password" name="password">
            </li>
            <li>
                <button type="submit">Register</button>
            </li>
        </ul>
    </form>
</body>
</html>