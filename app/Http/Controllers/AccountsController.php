<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Twitter\TwitterAPI;
use Illuminate\Http\Request;
use App\Twitter\TwitterAccount;
use App\Services\AddUserAccounts;

class AccountsController extends Controller
{
    protected $twitter;

    public function __construct(TwitterAPI $twitter)
    {
        $this->twitter = $twitter;
    }

    public function create($screenName)
    {
        $users = $this->twitter->find($screenName);
        $account = TwitterAccount::create($users->first());
        
        return response()->json(['data' => $account]);
    }
}
