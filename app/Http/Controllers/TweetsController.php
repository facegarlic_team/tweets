<?php

namespace App\Http\Controllers;

use App\Account;
use App\Http\Requests;
use App\Twitter\Tweets;
use App\Twitter\TwitterAPI;
use Illuminate\Http\Request;
use App\Services\RefreshTWeets;

class TweetsController extends Controller
{
    /**
     * TwitterAPI Wrapper
     * 
     * @var \App\Twitter\TwitterAPI
     */
    protected $twitter;

    /**
     * New TweetsController instance
     * 
     * @param TwitterAPI $twitter
     * @return void
     */
    public function __construct(TwitterAPI $twitter)
    {
        $this->twitter = $twitter;
    }

    /**
     * Handles the indexation of tweets for the given account
     * 
     * @param string $screenName
     * @return Response
     */
    public function create($screenName)
    {
        $account = Account::where('screen_name', $screenName)->first();

        if (!Tweets::exists($account->twitter_user_id)) {
            return $this->fetchFirstTimeTweets($account);
        }

        return $this->fetchNewTweets($account);
    }

    /**
     * Fetch the tweets for the first time
     * 
     * @param  Account $account
     * @return Response
     */
    private function fetchFirstTimeTweets(Account $account)
    {
        $tweets = $this->twitter->tweets($account->screen_name);

        if ($tweets->isEmpty()) {
            return response()->json(['message' => 'This account haven\'t tweeted yet.']);
        }

        for ($i = 0; $i < $this->twitter->maxTweetsAllowed(); $i++) {
            if ($tweets->isEmpty()) {
                return response()->json(['message' => 'Tweets successfully added.']);
            }
            Tweets::save($tweets, $account);

            $params = ['max_id' => $tweets->last()['id'] - 1];
            $tweets = $this->twitter->tweets($account->screen_name, $params);
        }
    }

    /**
     * Fetches new tweets from user timeline
     * 
     * @param  \App\Accouny $account
     * @return Response
     */
    private function fetchNewTweets($account)
    {
        $lastTweet = Tweets::last($account->twitter_user_id);
        
        $params = ['since_id' => $lastTweet->tweet_id];
        $tweets = $this->twitter->tweets($account->screen_name, $params);

        if ($tweets->isEmpty()) {
            return response()->json(['message' => 'No new tweets since last indexation.']);
        }

        for ($i = 0; $i < $this->twitter->maxTweetsAllowed(); $i++) {
            if ($tweets->isEmpty()) {
                return response()->json(['message' => 'Tweets successfully added.']);
            }
            Tweets::save($tweets, $account);

            $params = ['since_id' => $tweets->first()['id']];
            $tweets = $this->twitter->tweets($account->screen_name, $params);
        }
    }
}
