<?php

namespace App\Http\Controllers;

use Log;
use App\Http\Requests;
use App\Twitter\Friends;
use App\Twitter\TwitterAPI;
use Illuminate\Http\Request;

class FriendsController extends Controller
{
     /**
     * TwitterAPI Wrapper
     * 
     * @var \App\Twitter\TwitterAPI
     */
    protected $twitter;

    /**
     * New TweetsController instance
     * 
     * @param TwitterAPI $twitter
     * @return void
     */
    public function __construct(TwitterAPI $twitter)
    {
        $this->twitter = $twitter;
    }

    public function create($screenName, Request $request)
    {
        $params = $request->input();
        $friends = $this->twitter->friends($screenName, $params);

        if (empty($friends['users'])) {
            return response()->json(['message' => 'This account has no friends :(']);
        }

        try {
            $this->fetchFriends($screenName, $friends, $params);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()]);
        }

        return response()->json(['message' => 'No more friends to index']);
    }

    /**
     * [fetchFriends description]
     * 
     * @param  [type] $screenName [description]
     * @param  [type] $friends    [description]
     * @return [type]             [description]
     */
    private function fetchFriends($screenName, $friends, $params)
    {
        Friends::save($screenName, $friends->first());

        while ($friends['next_cursor'] != 0) {

            $params = array_merge($params, ['cursor' => $friends['next_cursor']]);
            
            Log::info('cursor: '. $friends['next_cursor']);
            Log::info('-------');
            
            $friends = $this->twitter->friends($screenName, $params);
            Friends::save($screenName, $friends->first());
        }

        // do {
        //     Friends::save($screenName, $friends->first());

        //     $params = array_merge($params, ['cursor' => $friends['next_cursor']]);
            
        //     Log::info('cursor: '. $friends['next_cursor']);
        //     Log::info('-------');
            
        //     $friends = $this->twitter->friends($screenName, $params);
        // } while ($friends['next_cursor'] != 0);
    }
}
