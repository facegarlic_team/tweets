<?php

$router->get('/', function() {
    return ['message' => 'welcome'];
});

$router->get('login', 'Auth\AuthController@create');
$router->post('login', 'Auth\AuthController@store');
$router->get('logout', 'Auth\AuthController@destroy');

$router->get('dashboard', ['middleware' => 'auth', function () {
    return ['You are logged in'];
}]);

$router->get('register', 'Auth\RegistrationController@create');
$router->post('register', 'Auth\RegistrationController@store');

$router->get("accounts/{account}", "AccountsController@create");
$router->get("accounts/{account}/tweets", "TweetsController@create");
$router->get("accounts/{account}/friends", "FriendsController@create");