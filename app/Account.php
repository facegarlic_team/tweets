<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = [
        'twitter_user_id',
        'name',
        'screen_name',
        'location',
        'description',
        'type',
        'utc_offset',
        'joined_at'
    ];

    // protected $dates = ['joined_at'];
}
