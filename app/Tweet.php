<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{
    protected $fillable = [
        'tweet_id',
        'account_id',
        'content',
        'mentions',
        'is_retweet',
        'retweet_count',
        'favorite_count',
        'tweeted_at',
        'utc_offset'
    ];

    public function hashtag()
    {
        return $this->belongsToMany(Hashtag::class);
    }
}
