<?php

namespace App\Twitter;

use App\Tweet;
use App\Hashtag;
use App\Account;
use Carbon\Carbon;

class Tweets
{
    /**
     * Handles the process of saving a tweet and its hashtags
     * 
     * @param  \Illuminate\Database\Eloquent\Collection  $tweetsFeed
     * @param  Account $account   
     * @return boolean
     */
    public static function save($tweetsFeed, Account $account)
    {
        $instance = new static;
        $instance->account = $account;

        $instance->mapTweetFeed($tweetsFeed)->map(function ($tweet) use ($instance) {
            $newTweet = Tweet::create($tweet);
            $instance->saveTweetHashtags($newTweet, $tweet['hashtags']);
        });

        return true;
    }

    /**
     * Checks if the given account already have tweets
     * 
     * @param  integer $accountId
     * @return boolean
     */
    public static function exists($accountId)
    {
        return Tweet::where('account_id', $accountId)->exists();
    }

    /**
     * Gets the last tweet in storage for the given account
     * 
     * @param  integer $value
     * @return \App\Tweet
     */
    public static function last($accountId)
    {
        return Tweet::where('account_id', $accountId)
            ->orderBy('tweeted_at', 'desc')
            ->first();
    }

    /**
     * Maps the tweet fields to our db fields
     * 
     * @param  array $tweet
     * @return void
     */
    private function mapTweetFeed($tweets)
    {
        return $tweets->map(function ($tweet) {
           return [
                'tweet_id' => $tweet['id'],
                'account_id' => $this->account->twitter_user_id,
                'content' => trim(preg_replace('/\s+/', ' ', $tweet['text'])),
                'mentions' => $this->extractMentions($tweet),
                'hashtags' => array_get($tweet, 'entities.hashtags'),
                'is_retweet' => array_key_exists('retweeted_status', $tweet),
                'retweet_count' => $tweet['retweet_count'],
                'favorite_count' => $tweet['favorite_count'],
                'tweeted_at' => (new Carbon($tweet['created_at']))->toDateTimeString(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];
        });
    }

    /**
     * Extracts the screen_name of the users mentioned in the tweet
     * 
     * @param array  $mentions
     * @return json
     */
    private function extractMentions($tweets)
    {
        $menttions = collect(array_get($tweets, 'entities.user_mentions'));

        return $menttions->map(function ($mention) {
            return $mention['screen_name'];
        })->toJson();
    }

    /**
     * Saves the tweet hashtags
     * 
     * @param  \App\Tweet $newTweet
     * @param  array $hashtags
     * @return void
     */
    private function saveTweetHashtags($newTweet, $hashtags)
    {
        foreach ($hashtags as $hashtag) {
            $hashtag = Hashtag::firstOrCreate(['hashtag' => $hashtag['text']]);
            $newTweet->hashtag()->attach($hashtag);
        }
    }

}