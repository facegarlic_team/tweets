<?php

namespace App\Twitter;

use App\Account;
use Carbon\Carbon;

class TwitterAccount
{
    /**
     * Saves in storage a twitter user account
     * 
     * @param  \Illuminate\Database\Eloquent\Collection $fields
     * @param  integer $type  
     * @return \App\Account|null
     */
    public static function create($user, $type = 0)
    {
        $instance = new static;
        $user = $instance->mapUserFields($user, $type);
        
        return Account::firstOrCreate($user);
    }

    /**
     * Maps the twitter feed user fiels to our database fields
     * 
     * @param  array $account
     * @param  int $type   
     * @return array
     */
    private function mapUserFields($account, $type)
    {
        return [
            'twitter_user_id' => $account['id'],
            'name' => $account['name'],
            'screen_name' => $account['screen_name'],
            'location' => $account['location'],
            'description' => $account['description'],
            'type' => $type,
            'utc_offset' => $account['utc_offset'],
            'joined_at' => (new Carbon($account['created_at']))->toDateString(),
        ];
    }
}