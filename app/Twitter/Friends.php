<?php 

namespace App\Twitter;

use DB;
use App\Account;
use Carbon\Carbon;

class Friends 
{
    /**
     * [save description]
     * 
     * @param  [type] $screenName [description]
     * @param  [type] $friends    [description]
     * @return [type]             [description]
     */
    public static function save($screenName, $friends)
    {
        $instance = new self;
        $account = Account::where('screen_name', $screenName)->first();

        return DB::table('friends')->insert($instance->mapFriendFields($friends, $account));
    }

    /**
     * [mapFriendFields description]
     * 
     * @param  [type] $friends [description]
     * @param  [type] $account [description]
     * @return [type]          [description]
     */
    private function mapFriendFields($friends, $account)
    {
        $mapedFriends = [];

        foreach ($friends as $friend) {
            $mapedFriends[] = [
                'account_id' => $account->twitter_user_id,
                'friend_id' => $friend['id'],
                'screen_name' => $friend['screen_name'],
                'location' => $friend['location'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
        }

        return $mapedFriends;
    }
}