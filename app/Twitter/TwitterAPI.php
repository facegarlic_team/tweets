<?php

namespace App\Twitter;

use GuzzleHttp\Client;

class TwitterAPI
{
    const MAX_TWEETS_ALLOWED = 3200;

    /**
     * Guzzle Client
     * 
     * @var GuzzleHttp\Client
     */
    protected $client;

    /**
     * Default query string parameters 
     * 
     * @var array
     */
    protected $defaultParams = [
        'include_rts' => 1,
        'trim_user' => 1,
        'count' => 200
    ];

    /**
     * New TwitterAPI instance
     * 
     * @param Client $client
     * @return void
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Finds a user information with the given screen name
     * 
     * @param  string $screenName
     * @param  array  $params    
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function find($screenName, $params = [])
    {
        $response = $this->client->get("users/lookup.json?screen_name={$screenName}");

        return $this->respondWithCollection($response);
    }

    /**
     * Fetch the tweets for the given account
     * 
     * @param  string $screenName
     * @param  array  $params
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function tweets($screenName, $params = [])
    {
        $params = http_build_query(array_merge($this->defaultParams, $params));
        $response = $this->client->get("statuses/user_timeline.json?screen_name={$screenName}&{$params}");
        
        return $this->respondWithCollection($response);
    }
    
    /**
     * [follows description]
     * 
     * @param  [type] $screenName [description]
     * @param  array  $params     [description]
     * @return [type]             [description]
     */
    public function friends($screenName, $params = [])
    {
        $params = http_build_query($params);
        $response = $this->client->get("friends/list.json?screen_name={$screenName}&{$params}");

        return $this->respondWithCollection($response);
    }

    /**
     * Gets the max allowed number of users
     * 
     * @return integer
     */
    public function maxTweetsAllowed()
    {
        return self::MAX_TWEETS_ALLOWED;
    }

    /**
     * Transforms the response array into a collection
     * 
     * @param  array $response
     * @return \Illuminate\Database\Eloquent\Collection
     */
    private function respondWithCollection($response)
    {
        return collect(
            json_decode($response->getBody()->getContents(), true)
        );
    }

}