<?php

namespace App\Providers;

use GuzzleHttp\Client;
use App\Twitter\TwitterAPI;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use Illuminate\Support\ServiceProvider;

class TwitterServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return \App\Twitter\TwitterAPI
     */
    public function register()
    {
        $this->app->bind(TwitterAPI::class, function () {
            $stack = $this->setUpOAuthMiddleware();
            $client = $this->createGuzzleClient($stack);

            return new TwitterAPI($client);
        });
    }

    /**
     * Setup the OAuth1 credentials and middleware 
     *
     * @return \GuzzleHttp\HandlerStack
     */
    protected function setUpOAuthMiddleware()
    {
        $stack = HandlerStack::create();
        $middleware = new Oauth1(config('services.twitter_api'));
        $stack->push($middleware);

        return $stack;
    }

    /**
     * Creates the Guzzle Client to interact with the Twitter API
     * 
     * @param  \GuzzleHttp\HandlerStack $stack
     * @return \GuzzleHttp\Client
     */
    protected function createGuzzleClient($stack)
    {
        return new Client([
            'base_uri' => 'https://api.twitter.com/1.1/',
            'handler' => $stack,
            'auth' => 'oauth'
        ]);
    }
}
