<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHashtagTweetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hashtag_tweet', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hashtag_id')->unsigned()->index();
            $table->integer('tweet_id')->unsigned()->index();

            $table->foreign('hashtag_id')->references('id')->on('hashtags')
                ->onUpdate('cascade')
                ->onDelete('restrict');

            $table->foreign('tweet_id')->references('id')->on('tweets')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hashtag_tweet', function (Blueprint $table) {
            $table->dropForeign('hashtag_tweet_hashtag_id_foreign');
            $table->dropForeign('hashtag_tweet_tweet_id_foreign');
        });
        
        Schema::drop('hashtag_tweet');
    }
}
