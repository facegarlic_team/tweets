<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTweetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tweets', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('tweet_id')->unsigned()->index();
            $table->bigInteger('account_id')->unsigned()->index();
            $table->text('content');
            $table->json('mentions')->nullable();
            $table->boolean('is_retweet')->default(false);
            $table->integer('retweet_count')->nullable();
            $table->integer('favorite_count')->nullable();
            $table->dateTime('tweeted_at');
            $table->timestamps();

            $table->foreign('account_id')->references('twitter_user_id')->on('accounts')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tweets', function (Blueprint $table) {
            $table->dropForeign('tweets_account_id_foreign');
        });

        Schema::drop('tweets');
    }
}
