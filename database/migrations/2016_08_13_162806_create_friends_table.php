<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFriendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('friends', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('account_id')->unsigned();
            $table->bigInteger('friend_id')->unsigned();
            $table->string('screen_name');
            $table->string('location')->nullable();
            $table->timestamps();
            
            $table->foreign('account_id')->references('twitter_user_id')->on('accounts')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('friends', function (Blueprint $table) {
            $table->dropForeign('friends_account_id_foreign');
        });

        Schema::drop('friends');
    }
}
